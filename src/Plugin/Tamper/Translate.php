<?php

namespace Drupal\tamper_translate\Plugin\Tamper;

use Drupal\file\Entity\File;
use Drupal\Core\Form\FormStateInterface;
use Drupal\tamper\Exception\TamperException;
use Drupal\tamper\TamperableItemInterface;
use Drupal\tamper\TamperBase;

use Google\Cloud\Translate\V2\TranslateClient;
use DeepL\Translator;

/**
 * Plugin implementation for translate.
 *
 * @Tamper(
 *   id = "translate",
 *   label = "Translate",
 *   description = "Translates string from one language to another.",
 *   category = "Text"
 * )
 */
class Translate extends TamperBase {

  private $google_translate = null;
  private $google_languages = null;

  private $deepl_translate = null;
  private $deepl_languages = null;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $services = [];

    // check availability, populate available languages
    if(!empty(\Drupal::config('tamper_translate.settings')->get('google_key_file_location')) && $this->getGoogleTranslateClient() !== null) {
      $services['google'] = 'Google';
    }

    if(trim(\Drupal::config('tamper_translate.settings')->get('deepl_api_key')) !== '' && $this->getDeeplTranslateClient() !== null) {
      $services['deepl'] = 'DeepL';
    }

    $form['ignore_empty'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ignore empty'),
      '#required' => false,
      '#default_value' => $this->getSetting('ignore_empty'),
      '#description' => 'Return empty string if input is empty or NULL'
    ];

    $form['service'] = [
      '#type' => 'select',
      '#title' => $this->t('Translation Service'),
      '#required' => true,
      '#options' => $services,
      '#default_value' => $this->getSetting('service'),
      '#description' => 'Translation Service to use'
    ];

    $form['output_lang_google'] = [
      '#type' => 'select',
      '#title' => $this->t('Output Language'),
      '#options' => $this->google_languages,
      '#default_value' => $this->getSetting('output_lang_google'),
      '#description' => '<a href="https://cloud.google.com/translate/docs/languages">Languages table</a>',
      '#states' => [
        'visible' => [
          'select[name="plugin_configuration[service]"]' => ['value' => 'google'],
        ],
        'required' => [
          'select[name="plugin_configuration[service]"]' => ['value' => 'google'],
        ]
      ],
    ];

    $form['output_lang_deepl'] = [
      '#type' => 'select',
      '#title' => $this->t('Output Language'),
      '#options' => $this->deepl_languages,
      '#default_value' => $this->getSetting('output_lang_deepl'),
      '#description' => '<a href="https://www.deepl.com/docs-api/translating-text/">Languages table (Parameter target_lang)</a>',
      '#states' => [
        'visible' => [
          'select[name="plugin_configuration[service]"]' => ['value' => 'deepl'],
        ],
        'required' => [
          'select[name="plugin_configuration[service]"]' => ['value' => 'deepl'],
        ]
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $this->setConfiguration([
      'ignore_empty' => $form_state->getValue('ignore_empty'),
    ]);
    $this->setConfiguration([
      'service' => $form_state->getValue('service'),
    ]);
    $this->setConfiguration([
      'output_lang_google' => $form_state->getValue('output_lang_google'),
    ]);
    $this->setConfiguration([
      'output_lang_deepl' => $form_state->getValue('output_lang_deepl'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function tamper($data, TamperableItemInterface $item = NULL) {
    if($this->getSetting('ignore_empty') === 1) {
      if(strlen($data) === 0 || is_null($data) || is_array($data)) {
        return '';
      }
    }

    if (!is_string($data)) {
      throw new TamperException('Input should be a string.');
    }

    if(!is_string($this->getSetting('service')) || $this->getSetting('service') == '') {
      throw new TamperException('No service set.');
    }

    $ret = '';

    switch($this->getSetting('service')) {
      case 'google':
        $result = $this->getGoogleTranslateClient()->translate($data, [
          'target' => $this->getSetting('output_lang_google'),
        ]);
        if(is_countable($result) && count($result) > 0) {
          $ret = $result['text'];
        } else {
          throw new TamperException("Google: No translation returned.");
        }
        break;

      case 'deepl':
        $result = $this->getDeeplTranslateClient()->translateText($data, null, $this->getSetting('output_lang_deepl')); // 2nd param null for input-lang auto detection
        if(is_object($result) && !empty($result->text)) {
          $ret = $result->text;
        } else {
          throw new TamperException("DeepL: No translation returned.");
        }
        break;

      default:
        throw new TamperException('Non-existing service selected.');
        break;
    }

    return html_entity_decode($ret);
  }

  /**
   * Returns Google Translate client if available, tries to create new one otherwise
   */
  public function getGoogleTranslateClient() {
    if($this->google_translate !== null) {
      return $this->google_translate;
    }

    $this->google_translate = new TranslateClient([
      'keyFilePath' => \Drupal::root() . '/' . \Drupal::config('tamper_translate.settings')->get('google_key_file_location'),
      'suppressKeyFileNotice' => true,
    ]);

    $this->google_languages = self::normalizeGoogleLanguages($this->google_translate->localizedLanguages([
      'target' => \Drupal::languageManager()->getCurrentLanguage()->getId()
    ]));

    return $this->google_translate;
  }

  /**
   * Formats Google API client's localizedLanguages to drupal form options
   */
  private static function normalizeGoogleLanguages($languages) {
    $languages_out = [];

    foreach ($languages as $lang) {
      $languages_out[$lang['code']] = $lang['name'];
    }

    return $languages_out;
  }

  /**
   * Returns DeepL client if available, tries to create new one otherwise
   */
  public function getDeeplTranslateClient() {
    if($this->deepl_translate !== null) {
      return $this->deepl_translate;
    }

    $authKey = \Drupal::config('tamper_translate.settings')->get('deepl_api_key'); // '02f70b85-846c-9aaa-f53d-4138010bf874';

    $this->deepl_translate = new Translator($authKey);

    try {
      $this->deepl_languages = self::normalizeDeeplLanguages($this->deepl_translate->getTargetLanguages());
    } catch(DeepLException $e) {
      if($e->getMessage() == 'The Response seems to not be valid JSON.') {
        \Drupal::messenger()->addError(t('DeepL response is not valid JSON, is the API key set and correct?'));

        return null;
      }

      throw $e;
    }

    return $this->deepl_translate;
  }

  /**
   * Formats DeepL client's languages to drupal form options
   */
  private static function normalizeDeeplLanguages($languages) {
    $languages_out = [];

    foreach ($languages as $lang) {
      $languages_out[$lang->code] = $lang->name;
    }

    return $languages_out;
  }
}
