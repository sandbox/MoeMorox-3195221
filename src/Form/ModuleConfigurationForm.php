<?php

namespace Drupal\tamper_translate\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * API key settings Configuration form
 */
class ModuleConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tamper_translate_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tamper_translate.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tamper_translate.settings');

    $form['deepl_api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('DeepL API key'),
      '#default_value' => $config->get('deepl_api_key'),
    ];

    $form['google_key_file_location'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Google key file location on fs relative to app root'),
      '#default_value' => $config->get('google_key_file_location'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('tamper_translate.settings')
      ->set('deepl_api_key', $form_state->getValue('deepl_api_key'))
      ->set('google_key_file_location', $form_state->getValue('google_key_file_location'))
      ->save();

    parent::submitForm($form, $form_state);
  }
}
